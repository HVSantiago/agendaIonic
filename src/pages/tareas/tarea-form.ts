import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { TareaService } from '../../app/services/TareaService';
import { TareasPage } from './tareas';

@Component({
  selector: 'page-tarea-form',
  templateUrl: 'tarea-form.html',
})
export class TareaFormPage implements OnInit {
  private parametro:string;
  private titulo1:string;

  private tarea:any = {
    titulo: "",
    descripcion: "",
    fechaFinal: "",
    estado: "",
    idTarea: 0
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public tareaService: TareaService
  ) {

    this.parametro = this.navParams.get('parametro');

    if(this.parametro != "nuevo") {
      this.tareaService.getTarea(this.parametro)
      .subscribe(res => this.tarea = res);
      this.titulo1 = "Detalle Tarea"
    } else {
      this.titulo1 = "Nuevo Tarea";
    }
  }

  ngOnInit() {}

  public guardar() {
    this.parametro = this.navParams.get('parametro');
    if (this.parametro === "nuevo") {
      this.tarea.fechaFinal = this.tarea.fechaFinal.substr(0,10);
      console.log(this.tarea.fechaFinal);
      this.tareaService.nuevaTarea(this.tarea)
      .subscribe(res => {
        this.toast.create({
          message: res.mensaje,
          duration: 2000
        }).present();

        setTimeout(() => {
          if(res.estado) {
            this.navCtrl.push(TareasPage);
          } else {
            this.tarea.titulo = "";
            this.tarea.descripcion = "";
            this.tarea.fechaFinal = "";
            this.tarea.estado = "";
          }
        }, 3000);
      });
    } else {
      this.tarea.fechaFinal = this.tarea.fechaFinal.substr(0,10);
      console.log(this.tarea.fechaFinal);
      this.tareaService.editarTarea(this.tarea)
      .subscribe(res => {
        this.toast.create({
          message: res.mensaje,
          duration: 2000
        }).present();

        setTimeout(() => {
          if(res.estado) {
            this.navCtrl.push(TareasPage);
          } else {
            this.tarea.titulo = "";
            this.tarea.descripcion = "";
            this.tarea.fechaFinal = "";
            this.tarea.estado = "";
          }
        }, 3000);
      });
    }
  }


}
