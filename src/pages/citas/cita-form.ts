import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { CitaService } from '../../app/services/CitaService';
import { CitasPage } from './citas';
import { ContactoService } from '../../app/services/ContactoService';

@Component({
  selector: 'page-cita-form',
  templateUrl: 'cita-form.html',
})
export class CitaFormPage implements OnInit {
  private parametro:string;
  private titulo1:string;
  private contactos:any[] = [];


  private cita:any = {
    asunto: "",
    descripcion: "",
    fechaCita: "",
    idContacto: 0
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public citaService: CitaService,
    public contactoService: ContactoService
  ) {
    this.inicializar();
    this.parametro = this.navParams.get('parametro');

    if(this.parametro != "nuevo") {
      this.citaService.getCita(this.parametro)
      .subscribe(res => this.cita = res);
      this.titulo1 = "Detalle Cita"
    } else {
      this.titulo1 = "Nuevo Cita";
    }
  }

  public inicializar(){
    this.contactoService.getContactos()
    .subscribe(contactos => this.contactos = contactos);
  }

  ngOnInit() {}

  public guardar() {
    this.parametro = this.navParams.get('parametro');
    if (this.parametro === "nuevo") {
      this.cita.fechaCita = this.cita.fechaCita.substr(0,10);
      console.log(this.cita.fechaCita);
      this.citaService.nuevaCita(this.cita)
      .subscribe(res => {
        this.toast.create({
          message: res.mensaje,
          duration: 2000
        }).present();

        setTimeout(() => {
          if(res.estado) {
            this.navCtrl.push(CitasPage);
          } else {
            this.cita.asunto = "";
            this.cita.descripcion = "";
            this.cita.fechaCita = "";
            this.cita.idContacto = ""
          }
        }, 3000);
      });
    } else {
      this.cita.fechaCita = this.cita.fechaCita.substr(0,10);
      console.log(this.cita.fechaCita);
      this.citaService.editarCita(this.cita)
      .subscribe(res => {
        this.toast.create({
          message: res.mensaje,
          duration: 2000
        }).present();

        setTimeout(() => {
          if(res.estado) {
            this.navCtrl.push(CitasPage);
          } else {
            this.cita.asunto = "";
            this.cita.descripcion = "";
            this.cita.fechaCita = "";
            this.cita.idContacto = ""
          }
        }, 3000);
      });
    }
  }
}
